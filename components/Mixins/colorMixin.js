export default {
  props: {
    color: {
      required: false,
      type: String,
      default: 'default',
    },
  },

  computed: {
    colorCalc () {
      return this.color.match(/^#[0-9a-f]{6}$/) ? this.color : `var(--${this.color})`
    },
  }
}
