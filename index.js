import JButton from './components/Template/Components/JButton'
import JDivider from './components/Template/Components/JDivider'
import JIcon from './components/Template/Components/JIcon'
import AppBar from './components/Template/Layout/AppBar/AppBar'
import AppBarActions from './components/Template/Layout/AppBar/AppBarActions'
import AppBarMainIcon from './components/Template/Layout/AppBar/AppBarMainIcon'
import AppBarTitle from './components/Template/Layout/AppBar/AppBarTitle'
import AppTemplate from './components/Template/Layout/AppTemplate/AppTemplate'
import NavigationDrawer from './components/Template/Layout/NavigationDrawer/NavigationDrawer'
import NavigationDrawerHeader from './components/Template/Layout/NavigationDrawer/NavigationDrawerHeader'
import NavigationDrawerHeaderIcon from './components/Template/Layout/NavigationDrawer/NavigationDrawerHeaderIcon'
import NavigationDrawerHeaderSubtitle
  from './components/Template/Layout/NavigationDrawer/NavigationDrawerHeaderSubtitle'
import NavigationDrawerHeaderTitle from './components/Template/Layout/NavigationDrawer/NavigationDrawerHeaderTitle'
import NavigationDrawerSection from './components/Template/Layout/NavigationDrawer/NavigationDrawerSection'
import NavigationDrawerSectionListItem
  from './components/Template/Layout/NavigationDrawer/NavigationDrawerSectionListItem'
import NavigationDrawerSectionList from './components/Template/Layout/NavigationDrawer/NavigationDrawerSectionList'
import NavigationDrawerSectionLabel from './components/Template/Layout/NavigationDrawer/NavigationDrawerSectionLabel'

export default {
  install (Vue) {
    Vue.mixin({
      components: {
        AppBar,
        AppBarActions,
        AppBarMainIcon,
        AppBarTitle,
        AppTemplate,
        NavigationDrawer,
        NavigationDrawerHeader,
        NavigationDrawerHeaderIcon,
        NavigationDrawerHeaderSubtitle,
        NavigationDrawerHeaderTitle,
        NavigationDrawerSection,
        NavigationDrawerSectionLabel,
        NavigationDrawerSectionList,
        NavigationDrawerSectionListItem,
        JButton,
        JDivider,
        JIcon,
      },
    })
  },
}
